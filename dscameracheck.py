
# we write a frame number every 8 or 9ms
# when we push the button in the GUI, we raise lines 20 and 21.   These are pins 38 and 40.
# Attach one to the PIR input.  Attach the other to an oscilloscope
#don't forget to GROUND your board on pin 6.

import Tkinter as tk
import sys
import RPi.GPIO as GPIO

# Note: Python 2.6 or higher is required for .format() to work
 
frame_count=0
three_times=[8,8,9]
passcount=0
 
def update_frameText():
    global state
    if (state):
        global frame_count
        global three_times
        global passcount
# we 8.333ms, but out counter isn't so granular, so we go 8ms, 8ms, 9ms, which should be good enough       
        if( passcount < 2 ):
            passcount += 1
        else:
            passcount = 0
        frame_count += 1   
#turn off the PID after 10 frames 
        if(frame_count > 10):
			GPIO.output(20,False)
			GPIO.output(21,False)
        frameString = pattern.format(frame_count)
        frameText.configure(text=frameString)
    if (frame_count < 350):
		root.after(three_times[passcount], update_frameText)
 
def start():
    global state
    GPIO.output(20,True)
    GPIO.output(21,True)
    frame_count = 0
    state = True
 

def reset():
    global frame_count
    frame_count=0
    frameText.configure(text='0')
    global state
    state = False
 
def exist():
    root.destroy()
    GPIO.cleanup()
     
# setup
GPIO.VERSION
GPIO.setmode(GPIO.BCM)
GPIO.setup(20, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
PIO.output(21,False)
 
state = False
root = tk.Tk()
root.wm_title('Deep Sentinel Camera Test App')
root.geometry("1000x250")
ll = tk.Label(root, text='lower left')
lr = tk.Label(root, text='lower right')
##ll.place(x=0, y=250, anchor='sw')
##lr.place(x=1000, y=250, anchor='se')
ll.place(relx=0.0, rely=1.0, anchor='sw')
lr.place(relx=1.0, rely=1.0, anchor='se')
pattern = '{0:03d}'
frameText = tk.Label(root, text="0", font=("Helvetica", 150))
frameText.pack()
startButton = tk.Button(root, text='Start', command=start)
startButton.pack()
resetButton = tk.Button(root, text='Reset', command=reset)
resetButton.pack()
quitButton = tk.Button(root, text='Quit', command=exist)
quitButton.pack()
update_frameText()
 
root.mainloop()




